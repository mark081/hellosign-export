const fs = require("fs");
const Database = require("better-sqlite3");
const hellosign = require("hellosign-sdk")({
  key: "",
});

let ERR_COUNT = 0;
let FILE_COUNT = 0;

const prepDB = (page, db_name) => {
  //    const db = new Database('./hellosign9.db', { verbose: console.log });
  const db = new Database(db_name);
  if (page === 1) {
    try {
      const drop = db.prepare(`DROP TABLE documents`);
      drop.run();
    } catch (err) {
      console.error(err);
    }
    try {
      const create = db.prepare(`CREATE TABLE documents (
                req_no   INT,
                req_id   STRING,
                title    STRING,
                filename STRING)`);
      create.run();
    } catch (err) {
      console.error(err);
    }
  }
  return db;
};

async function getTotalPages(opts) {
  const res = await hellosign.signatureRequest.list(opts);
  let { num_pages } = res.list_info;
  return num_pages;
}

exports.getTotalPages = getTotalPages;

exports.loadSignatureRequestIDs = (opts, db_name) => {
  let { page } = opts;
  const db = prepDB(page, db_name);
  const insert = db.prepare(
    `INSERT INTO documents (req_id, req_no, title) VALUES (?, ?, ?)`
  );
  hellosign.signatureRequest
    .list(opts)
    .then((res) => {
      res.signature_requests.forEach((x) => {
        let info = insert.run(x.signature_request_id, page, x.title);
        console.log(
          `INSERT_RUN returned changes: ${info.changes} lastInsertRowid: ${info.lastInsertRowid}`
        );
      });
    })
    .catch((err) => {
      console.error(`SIG_REQ_ERR ${err.message}`);
    });
};

exports.downloadFromSignatureRequestIDs = (opts, db_name, ms_wait) => {
  const db = new Database(db_name);
  const stmt = db.prepare("SELECT * FROM documents");
  const signatureRequestIds = stmt.all();

  const update = db.prepare(`update documents set filename = ? where req_no = ?`)
  let time_offset = ms_wait;

  signatureRequestIds.forEach((document) => {
    setTimeout(
      (document) => {
        try {
        hellosign.signatureRequest.download(
          document.req_id,
          { file_type: "zip" },
          (err, res) => {
            if (err != null) {
              console.log(
                `SIG_REQ ${err} error count no ${ERR_COUNT}`
              );
              ERR_COUNT++;
            } else {
              try {
                FILE_COUNT++;
                const file = fs.createWriteStream(
                  "./files/" + document.req_no + ".zip"
                );
                res.pipe(file);
                file.on("finish", () => {
                  file.close();
                });
                update.run(document.req_no + ".zip", document.req_no);
              } catch (e) {
                console.error(`GET_DOC ${e.message} RES: ${res}`);
              }
            }
          }
        );
        } catch (err) {
          console.error(err)
        }
      },
      time_offset,
      document
    );
    time_offset += ms_wait;
  });
};
