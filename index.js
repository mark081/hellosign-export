const {loadSignatureRequestIDs, getTotalPages, downloadFromSignatureRequestIDs} = require('./hs-mod1');

const PAGE_SIZE = 1
const MS_WAIT = 1250
/**
 * Hellosign only allows 25 downloads a minute(60000ms) so setting rate to one document per 2400ms
 */
const DL_WAIT = 2400
const DB_NAME = './hellosignFinal2.db'

if (process.argv.length >= 3) {
    switch(process.argv[2]) {
       case `-t`:
          getTotalPages({ account_id: 'all', page_size: 1 })
          .then((total_pages) => console.log(`Total Pages: ${total_pages}`));
          break;
       case `-l`:
        getTotalPages({ account_id: 'all', page_size: PAGE_SIZE })
        .then((count) => {
            let page=1 
            let waittime=0     
            for (i = count; i > 0; i--) {
                waittime = waittime +  MS_WAIT
                console.log(`waiting ${waittime}ms`)
                setTimeout(
                    () => {
                        loadSignatureRequestIDs({ account_id: 'all', page, page_size: PAGE_SIZE}, DB_NAME)
                        page = page + PAGE_SIZE;
                         },waittime
                    )
                }
            });
            break;
        case `-d`:
            downloadFromSignatureRequestIDs({ account_id: 'all', page_size: PAGE_SIZE }, DB_NAME, DL_WAIT)
            break;
       default:
          console.log(`Invalid Command ${process.argv[2]}`)
    }
} else {
    console.log("usage: -t [get total pages] | -l [load available pages] | -d [download available pages]")
}

